# Image config
HAPROXY_IMAGE = haproxy
HAPROXY_TAG = 2.0.12-alpine
BASE_IMAGE = registry.gitlab.com/andrewheberle/docker-base/ssl-confd
BASE_TAG = 3.11

# Registry path for tagging image
REGISTRY = registry.gitlab.com/andrewheberle

# Image information/metadata
IMAGE_ARCH = amd64
IMAGE_AUTHOR = Andrew Heberle
IMAGE_DESCRIPTION = HAProxy Container
IMAGE_NAME = docker-haproxy
IMAGE_VCS_BASE_URL = gitlab.com/andrewheberle

# Additional docker build options
DOCKER_BUILD_OPTS = --pull

# Values to replace in the Dockerfile
DOCKERFILE_REPLACE_VARS = HAPROXY_IMAGE HAPROXY_TAG BASE_IMAGE BASE_TAG IMAGE_ARCH IMAGE_AUTHOR IMAGE_DESCRIPTION IMAGE_NAME IMAGE_VCS_BASE_URL
